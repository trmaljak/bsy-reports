# BSY - task 10

In this repository, there are four reports (each for one assignment), that we did in the course on System Security. We took this course as a part of our Master studies at the Czech Technical University in Prague. 

The objective of these assignments was to demonstrate various techniques used in computer security.

## The task
The assignment number 10 is to write a report on your experiences during the class on the different topics we were doing. You must choose at least 4 different attack or defense experiences/assignments and you need to tell us in a blog what did you do to solve them. We want your **own personal experience and commands**.

You must send it as Markdown.

There is no pioneer prize for this assignment.

Submit to lukasond@fel.cvut.cz cc to sebastian.garcia@agents.fel.cvut.cz and rigakmar@fel.cvut.cz

## Reports
* [Report 04](report04.md)
* [Report 09c](report09c.md)
* [Report 07](lab7.md)
* [Report 03](lab3.md)

## Authors

Members of Team 8:

| Name           | Email                |
|----------------|----------------------|
| Tomáš Hořovský | horovtom@fel.cvut.cz |
| Jakub Trmal    | trmaljak@fel.cvut.cz |