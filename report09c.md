# Task 09c report

The task was to observe traffic on our VM and find the flag.

## Analyzing traffic
While going through the pcap file, that I captured on our VM, I found DNS UDP requests, that come from `192.168.1.223`. I found this suspicious, so I scanned all the ports using nmap. I was searching for any unusual service, or something else that would help me. I found a classic SSH that ran on port `22`, but also there was port `6666`, where (according to nmap), there was **irc** service. 

## Getting in
I used `ncat` on the port `6666` to get to the VM. After a while, I got to an emulated terminal. Using this terminal, I did a bit of searching, but gave up, because it was very tedious. Then I started looking for some way to make it easier and I realized that I was logged in as root in the terminal. Knowing this, I immediately created my own user `fantomas` on the machine and got access via regular ssh, that was running on port `22`. 

## Scraping the filesystem

First, I tried to get all changes in the filesystem for the last 7 days, using:

```bash
find / -type f -mtime -7 | grep -v "Permission"  > last7days.txt
```

Then I filtered out some files that were obviously not important:

```bash
less last7days.txt  | grep -v "/sys/" | grep -v "/proc/"  | less
```

Just looking at the files, I found an unusual program that can display images in the terminal: `/usr/local/bin/tiv`. I figured, that such a program would not be on the machine by accident. I started searching for some changes in images or pdf files:

```bash
less last7days.txt  | grep -v "/sys/" | grep -v "/proc"  | egrep ".jpg|.png|.bmp|.pdf" | less
```

As I expected, I found an image and I tried to open it using the program, that I found on the machine:

```bash
/usr/local/bin/tiv /opt/isengard/dnscat2/client/SarumanSuperSecretChest/TotalyNotAToken.jpg
```

However this did not work as intended, so I did it the old-fashioned way and downloaded the image to my machine using scp:

```bash
scp fantomas@192.168.1.223:/opt/isengard/dnscat2/client/SarumanSuperSecretChest/TotalyNotAToken.jpg .
```

After opening the image, I immediately found the token: `NobodyTossesADwarf`:

![img](TotalyNotAToken.jpg)

Then I just pasted the token to the submission server and got the flag as a result:

```bash
root@ubuntu8:/home/kuba/lab11# ncat 192.168.1.167 8993

                                  ....
                                .'' .'''
.                             .'   :
\                          .:    :
 \                        _:    :       ..----.._
  \                    .:::.....:::.. .'         ''.
   \                 .'  #-. .-######'     #        '.
    \                 '.##'/ ' ################       :
     \                  #####################         :
      \               ..##.-.#### .''''###'.._        :
       \             :--:########:            '.    .' :
        \..__...--.. :--:#######.'   '.         '.     :
        :     :  : : '':'-:'':'::        .         '.  .'
        '---'''..: :    ':    '..'''.      '.        :'
           \  :: : :     '      ''''''.     '.      .:
            \ ::  : :     '            '.      '      :
             \::   : :           ....' ..:       '     '.
              \::  : :    .....####\ .~~.:.             :
               \':.:.:.:'#########.===. ~ |.'-.   . '''.. :
                \    .'  ########## \ \ _.' '. '-.       '''.
                :\  :     ########   \ \      '.  '-.        :
               :  \'    '   #### :    \ \      :.    '-.      :
              :  .'\   :'  :     :     \ \       :      '-.    :
             : .'  .\  '  :      :     :\ \       :        '.   :
             ::   :  \'  :.      :     : \ \      :          '. :
             ::. :    \  : :      :    ;  \ \     :           '.:
              : ':    '\ :  :     :     :  \:\     :        ..'
                 :    ' \ :        :     ;  \|      :   .'''
                 '.   '  \:                         :.''
                  .:..... \:       :            ..''
                 '._____|'.\......'''''''.:..'''


***************************************************************
Flag server for assignment 9c. Not relevant to other assignemnts.
****************************************************************


<Gandalf>All that is gold does not glitter, not all those who wander are lost.
<Gandalf> Have you found the token?
NobodyTossesADwarf
<Gandalf> One Flag to rule them all, One Flag to find them, One Flag to bring them all, and in the darkness bind them: BSY-FLAG-A09C-{2fA932BF4dd696fDd8Ab67072229F5a35Ec2CEaD9cD3Af84C79D39bBf3fc55cD}
```
