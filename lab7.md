
# Lab7

In this assignment we were given a PCAP file and a submission server ip. The objective was to answer questions on the submission server.

After downloading the PCAP file, I connected to the submission server to see what should I be looking for.

```{shell}
class@ubuntu8:/home$ ncat 192.168.1.167 8888

....      ............          ...................................'''',,,,;;;;;,,,,,,,,,,,,,,;;;;,,,,,,,,,,,,,,''''....
...       ...........          ..................................''',,,,;;;;;;;;;,,,,,'''',,,,,,,,,,,,,,,,,,,,,,,'''''..
           .........           .................................'',,,;;;:::::::;;;,,,,''''',,,,,,,,,,,,,,,,,,,,,,,,'''..
           ........           ...............................''',,,;;:::cccccc:::;;;;,,,,,,,,,,,,,,''',,,,,,,,,,,,'''''.
            ...               ...............'''''''''''''',,,,;;::::ccccclllccccccc::;;;,,;;;;,,,,,,''''''''',,,,'''''.
                             ...............'''''''',,,,,,;;;;::ccccllllllllollllllllccc:::;;::;;;;;,,,,,,,''',,,,'''''.
                             ...............'''''''',,,;;;;::::ccllllllooooodxxxxdddxxxdollcccc::::::;;;,,,,,,,,,,,,,'''
                            ....................''''',,,,;;;;::ccclllllodxk0KNNNNXXXNWWNXK0Oxolllccccc:::;;;;;;;;;;;;;,,
                           ...........................''',,,;;;;:::cccokKNWWNNXXXXKKXXNNNWWWNXOdolllllcccc::::::::::::;;
                           ..............................''''',,,,;:ok0KXXNNXK000OOOO0000KKKXNNXOdoooollllllccccccccccc:
                         ........................................,lOXNKO00KK0OOOkxxxxxkkkkOOO0KNN0dllloolllllllllcclllcc
                        ........................................'lkkO0OOOO00OkkxdooloooddxkOOKXNWNKxllllllllcclllcccllcc
                       ........................................,lxdodddxkO00OkxoollllllloodkO0KXXKKOlc:::::::ccccccccccc
.............        ..........................................,okxooddxkkkkkxdolllllllllloddxkOOkkxc;;,,;;;;;;;;;;:::::
.................   ...........................................:ddoodxxkkkxddooooooddolloxkxxdxddxxd:''''''''''',,,,,,,,
.................. .............................'',,;:c:,'.....,:codddooooolllldxkO0Okxdddol:::cccl:,..............'''''
...............................................',,;;coxxxdl::::;:cll:;;:llclodxkO00Okxdoll:''',,,;,'....................
.............  ......................................',,;:cccllccccccc:cc::cldxO0KXKOxdool;'......'.....................
.................................................''''''''''''',,,,;::cdxdlloodxkO0KK0Oxddl;,'''.''......................
.........................................''''''''''''''''',,,,,;:cdxkO0KKK00O00K00000Okkxxoocc:::::;;;,,'...............
.......................................'''''''''''''''',,,;::cdk0XXNWNWWWWNNNWWNNNXKK00000d:;::;:::ccccc:::::;;;;;,'....
....................................'''''''''''''''''',;:ldk00XXXNNNNNNWNNNNWNNNWWWNNNXXXNXOxdl:,,,''''',,,,;;;;:;,'''..
...................................''''''''''''''''',,;lx0KXXXKKKKKKKKXXXNNNXXXXXXNWWWWNWWWWWN0x:,,,,,,,,'''',''''''''''
.....................''''''''''''''''''''''''''''',,,cx0KKK00OOkkkkkkO000KKKKKKKXXNNNWWWWWWWWMMW0l;,,,,,,,,,,,,,,,,,,,,'
..................'''''''',,'''''''''''''''''''',,;:ok000OkddolcccclloddxxkOO00KKKKXNNNNNWWWWWWMWKo;;;;;,,,,,,,,,,,;;;;,
..............'''',,,,,,,,,,,,,,,,,''''''''',,,,cdkxO00Oxdc:;;:::::ccllllooddxkkkOO00KKXXNNWWNWMMWKo;;;;;;;;;;;;;;;;;;;;
...........''',,,,;;;;;;;;;;;;;,,,,,,,,,,,,,,,,,dXNK0kkdl;,;;;;;;:::cclclllooodxxkkkkkkOO0XWWXNWMMWOc:;;;;;;;;;;::::::::
.........''',,;;;;:::::::::::::;;;;;,,,,,,,,,;;:dKNNX0kl:;,;;;;;;;:::ccccclloooddxxkxxdddxkKXXNWMMWXx:::::::::::::::::::
.....'''',,,;;::::cccccccccccc:::::;;;;;;;;;;;;:ckKXNNNKkl;;;;;;;;;::::ccccclllloooooooooooxOKXNWWWWOc::::::::::::::::::
'''',,,,;;;::cccclllllllllllllccccc:::::::::::::lxO000KXX0kdol:;;::::::ccccccclllllllllllllldO0KXNWWKo:ccccccccc:c::::::
,;;;;;;:::ccclllllooooooooooollllllcccccccccccccoxxdxkOOO000K0OdolcccccccccllcclcccccllcccccldO0KNNNXkollcccccccccc:::;;
:::::cccclllloooooooooooooooooooooollllllllllllldxo;;lodxxxxkkO0Oxdlllllcclllllllcccccccccccldk0KXNXXOollllllllllccc::;;
ccccllllloooooddooddddddddddddddddooooooooooooodxxc'',,,;::ccllodxxolllcclodxxkkkkxxxddooollldO0KXNNNOlcllllllllllccc::;
llllooooooddddddddddddddddddddddddddddddddddddddxd;','''...',,,,;;:cc::clodxkkO0KKXXXXXXXK0OOOKKXNNNN0olllooooooollcc::;
ooooooooddddddddddddddddxxxxxxxxxxxxxxxddddxxxxdoc,',,,'.'....''''',;;;:::::codxkO0KXXXXXXXXXXXXXXXXXOollooooooooolllc::
oooddddddddxxxxxxdddddddxxxxxxxxxxxxxxxxxxxxxxxo:;,,,,,,',;,,,;,...';:;,,'''',:cldxO0KKKKKKKKKKKKXKKKxoooooddddddooollc:
dddddddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxkkkxo:,,;;;;;;;,,,,;,,',;cc;,''.....'';:codkOOO0OOOO0KKKX0dooddddddddddooollc
ddddddxxxxxxxxxxxxxxxxxxxxkkkkkkkxkkkkkkkkkkkkxo:',,;:::cc::::;;;;cllc:;;,''''',,'',;;:cccccclxO0KX0xddddddddddddddoooll
dddddddxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxl,',,;::ccc:::;,;:cloolcc::;;;;::;;,;;;;;;;;;cok0KKklldddddddddddddddoool
dddddddxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxc''',,;;::;;,,,,;clodoc:::::c:::cc:::::::;;;:lxkOxo::ldddddddddddddddoool
ooooddddxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOx;'''',,;;;;;;;;,;:clolc::::::cclllllccc::;;;cdxo:;;:ldddddddddddddddooool
llloooodddxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOo,''',,,,;;::;;,'.,:::;:c::;:::clolllccc::::cdkd:,;:oxxxxxddddddddddoooooo
cccclloooodddxxxxxxxxxxxxxxxxxxxxkkkkkkkkkkkOOo,',,,,;:clllllc:;;;;;,;clc:;;:ccllccc::::::cxkdl::lxxxxxxxddddddooooooool
;;:::ccllloooddddddddddddddddddxxxxxxxxkkkkkkkd;',,,:lodddddooolcccooooooc:;;;::::::::::::ckkdoloxxxxxxxxxdddddddooooool
',,,;;:::cccllloooooooooooooooodddddxxxxxxxkkkx:',';lddooc;,,,;;:ccoxxddddl:;;::::;;;:::::oOkddxxkkkxxxxxxxxdddddddooooo
....'',,,;;::cccllllccccccccclllloooddddddxxxxxl;',:odool,.   ....'';loooddoc:::::::::::lxkOOkkkkkkkkkkkxxxxxxxddddddddo
.........'',,;;:::::::;;;;;;::::ccllllooooodxxxdc,;ldddo:,''..........:ccldxoc::::::::coxkO0OOkkkkkkkkkkkkkkxxxxxxxxxddd
.............',,,,,,;,,'''',,,;;;;::cllooodxkkxxo::oxxxo:;,,'',,,;,,;;:lllodolc::::::coxxk00Okkkkkkkkkkkkkkkkkkkxxxxxxxx
...............'''''''....''''',,,;:cclooddoodxxl:ldxkxocc::::;;;;;::cclllodolcc:::;:oxdxk000kxxkkkkkkkkkkkkkkkkkkkkkxxx
..............................''',,;:cclllooxxxxdccoxkkxdddddollcc:cccccooddollc::;;cododO00Oxxxxxxkkkkkkkkkkkkkkkkkkkkx
''.'''..........................'',:clllokkdxkkxxdlcdOOkxkkkkxxdoollolloddxdolc:;;;;odoodOK0kddddxxxxxkkkkkkkkkkkkkkkkkk
''',,,''',,'....................'',,;cdk00xxOkxxxxdook00kkOOkkkkxxxxxxxxxxxxocc:;;;cllllx0KOdoooddddxxxxxxkkkkkkkkkkkkkk
,',,,,',,;,,'''...................,cx0XXK00OkkxxxxxdodO00OOkkkkOkkkOOkkkkxxkdlc::;:lccclk0Kklllloooodddddxxxxxxxxxxkkkkx
''',,,,,,;;,''''.............';cox0NWNNNNKkkkkxxxxxxdddO000OOkkOOkkO0OkkkxxOkdol::oxl:coOK0dc::ccclllooooddddddddxxxxxxx
.....''',,;,,,''........,coxOKXWWNXXKKKXN0xdkkkkxxxxddoox0KKK00000O0KK00OkxxO0kddddxo:cx0KOc;;::cloooooooolllooooooooooo
.........',,,,''...''''.':xXWWWWWWWWNNXXKxooxkkkkxxxxddoooxO0KKKKKKKKXXXXXK0KXXXXKOdlcoOKKkllodxO0KXXXXXKKOxolcccclllllc
.........'','''',;::'..    :0WWWWMWWWNX0xolodxxkkxxxxxdddoolloxkk0KXKKKXXXXNNNNWWWNNKO0KXXKKKXNNNNNNNNNNNNNNX0kdlc::;;;;
.........'''''';ccc,. ..    ,ONWWMMWWXOdlooloxkxxxxxxxxddddoooll::coddkOKKKXXXXXXXNNNNNWWWWWWMMMMMMMMWWWWWNNNNNX0xl:,'''
.........',;;::ccc:;.....    ;0NNNX0kdollloooxkkxxkxdxxxxddddddddc,.''',:lxOO0KXXXXXXXXXNNNNWWWWWMMMMMMMMMMMMWWWNXKOdl::
.......',;;,,,:ccc:;'....    .l00Oxollllllooodxkkddxxxxxxxxxxxxxdxl'.'';ldxkk0XNWWWNNNNNNNNNNNNNNNNNNNNNNNNNNWWWNKOxlcc:
.....:dkOOOOkdl;,;;,'...      .dOkdoooollloooodkOkddddxxxxxxxxkkxxd,.,lxkkkkOKKXNWWK0KXWMMMWWWWWWWWWWNNNNNXXOxolc;,'''''
..'cONWNX00OO0KOo,.....        ,kkdddoodddddddoxOOOkkxxxxxxxkkkkkxdllxxxkkkOOkkKNWWNXKXNWMMWOoodkkkkkxxkkOOx:,''''..''..
'lONWMWNNXXK000KXO;..          .lkxxxxxxxxxxxxddOOOOOkkkkxxxxxxxddxkkkkkkkkxdoONWWMMWXXNWMMW0;...'........';;'';:c:,....
dkOOKNWWXKKKK000KXO,            'dkxxkkkxxxxxxddkOOkkkkkOOOkkkxdoodxxkkkkkOxox0NMMWWXXKKXNNNXx. ..    .......,xKNXl....'
kkkOOKXNNNXK00OO00Kk'           .cxxkkkkkxxxxxxdkkOOOOOOOOOOOOkkxdodxk0K000OddOXWWWNXXKKKXNNNXd. .        .. ;0XNNd.....
K00KKKKKXXNNXK0OOO0Kd.           ,dxkkkkkkkxxxxdxkOOOOOOOOOOkkOOxxO0OkOKK000OdxKWWWWNNNWMMMMMMNo.         ...c0XNNKxoo;.
00000OOOOO0KXXKKOkkO0o.          .oxkkkkkOkxxkkdxkkkkOOOOOOkkOOkxkKK0kk0XKO00xo0WWWWWWWMMMMMMMM0'           .o00KK000Ko.
KKKKK00000OOOKKXKOkkO0l.         .oxkO0OOOOkkkkdxxxkkOOOOOkkOOkdk0KK0OkkKK0O0OoOWWMWWWWMMMMMMWWNc           'dOkkOOxool'
k0XNNNXKXXXK000KK0OkkOO;         .lxk0KKKKOkkkkxxxxkOOOOOOkO0OxxO0KK0OkkOKK00OoONNNNNNXXXNNXNNNWo           ,dkxxxxxc'..
kOO0KXXXXXXXXKKKK0OkxkOd.         ckOKKKXK0OOOOkxxxkOOOOOkkOOkk000KK0Okxk0KK0koONXXKXKOO0KKXNNWWo          .:xxdoodxxl;,
00OOkO0KXKKKKKKKK0OkxxOO;         :kOKKXXKKOkOOkkddxOOOOkkkkkO0000000Okxk0XXKkd0NKKKXKO0KXXXXNNWo          .lddolldxkxdd
O00K00OO0KKKKKKKK0kxdxkOl         ;kOKXXXX0OkkkkxddkOOOkkkkkOO000000OkxxxOKKXOdKXXXXXK0KXNXXXXNNo          ,ldolclodkdod
dxxdxk0K000KKKKKK0OxddxOd.        'dkKXXXK0OOOOOkddkOOkkxxO00OOO000OOkxxxk0KKkx0KKKXXK0KXNXXXXXXl          ;lolc:cldxdoo
kkxdoloxO0KKKK00000xodxOx'        .okOKXX0OkkkkkkddkOkxxxOO0OOOOO00Okkxxdx0KKkx00O0KK0O0XNNXXXXKc         .;llc:;:coxdlc
KK0OkdlclxOKK0OO000kooxkd.        .okOKXK0kxxxxxxxxkkxdxkOOOOOOOOOOkkkxddxO0Kxd00OOKK0O0XNNXXXXK:         .,cc:;;;cdxo::
00KKK0OdccokO0OOOO0Odoxkc         .l0KXXKOxxxxdolodkkxkOOOOOOOOOOOOkkkxdodk00ddOOO0KKKKXNNNXXXX0;          ,cc:;,:oxdoc:
dxxO0K0Odc:oxOOOOO0Oxoxo.          l00KKOkdxkxdoc:loooxkkOOOkkOOOOOkxkxdook00dd0OOO0KKKXNNXXXKXk'          '::;,;oolool:
kxdddk00xl:cokOOOOOOxod,           :OO00kxdxkkkxoc:clooodkkkkOOOOOkxxkxoloxOOox0OOO0000KXXXXXKKd.          .;;,;clcclool
0OkdooxOxlc:ldkOOkOOdoc.           ,xk00kddddxxxxdoodddooooddxkkkkkxxxxollxOkoxOkkkO000XNXXKKKKo.          .,,,:lc;:clol
0OOxdooxxl::ldxkOkkkdo,            ,xO00kxoddddxxxxdxxxxxdddooooodxxxxxolldOkokOxk000KXNNNXXKK0c           .,,;cc;;:ccll
0Okxdllodoc:coxkOkkxdc.            ,xOOOkdoddxxxxxkxxxkxxxxxxxxdollodddoccdOxdkOk0KK0KNNNNXKKKO;           .',;c;,;::c::
OOkxollodl::codkkkkxo,             ,dxkkkdddxxxdddxxxkkxxxxxxxxxxxxxdddo:cdkdd0KKXXX0KXNNXXK00x'           .,;:,.,;:::::
kkkxolllol::cldxkkxxc.             ,dxkOxddxxdooddddxxxxxxxxxkkkkkOOdodo::dkooOO0KKK00KXXK00OOl.          .,;,...',;:::;
kxxdlcllol::cloxkkxl.              'dkkxooddoolllodxxxxxxxxxkkkkkkOOdodo::oxlo00OOOOkkOOOOOkxx:         .........',,;;;;
kxdolccllc:cclodxxo,               'odoodddollloxk00kxxxdddxxxkkkxkkdodo::ddcoOOOOOkkxxkkkxxdl'       ...   ......',,;,,
xxolccclc::ccloodd:.               'oooddolloodx0KKKOxdddddxxxkkkxkkdooo;:ddcokkkkkxxxxxxddddc.       ..    .. ...',,,,,
dolcccllc::cclllol.                'ooddollldxxkO000Odooooddxxxxxxxkdool;:docoxkOOkxxddddxxdd:.      .      .  ....'',,'
olc:cccc:::ccclll,                 .ldooooddxOOOOOkk0kolllodxxddddxxdool;;oclxxkkkxxddddxddoo,      .          .....''''
lc:::ccc::::cccc:.                 .ldooooxkOOO0OkkOKOo:clodddddoddxolol;:o:codxxxdooooodoll:.                 ......'''
c:::ccc::::::cc:.                  .colodxkOOOkOOkkO00o;:lddddddodddolol;:l;:llddxdooooooolc,                  .........
:;:::c:::::::::'                   .cddxOOOOOkxkkkkO0Kx;;loodddoooddolol;:c;cllodxdooollllc:.                  .........
;;;::::;;;;:::'                    .cxkkkkOkxxdxkkkO0KOc:loodoooooodolll;;;:lccloddoolcccc:,.                  .........
;;;:::;;;;;;;,.                     cOOOOOkxxdxxxkOOO00o:looooooooodollc;;;:c:ccloololcccc:'                   .........


***************************************************************
Flag server for assignment 8. Not relevant to other assignemnts.
****************************************************************


<Pai Mei> So my pathetic friend... is there anything that you can do well?
<Pai Mei> How many HTTP requests did 10.0.2.5 perform?

<Pai Mei> What is the IP address of the attacker?

<Pai Mei> How many open ports did the port scan find?

<Pai Mei> Did the attacker manage to break in the server? (If yes, write the first command they executed, if no answer with No)

<Pai Mei> Your so-called kung-fu... is really... quite pathetic.


Ncat: Broken pipe.
```

So we have to find answers to these questions:
1. How many HTTP requests did 10.0.2.5 perform?
2. What is the IP address of the attacker?
3. How many open ports did the port scan find?
4. Did the attacker manage to break in the server? (If yes, write the first command they executed, if no answer with No)

## 1: HTTP requests
To filter only requests that came from 10.0.2.5, we can use the filter `ip.src == 10.0.2.5`. However there are still some TCP packets, ssh packets and others that are of no interest to us. To filter those out, we can use the `http` filter with our ip filter.
![http_requests](7_http_requests.png)

I counted 19 requests, lets remember it.

## 2: Attackers IP
Just looking at the pcap file it is obvious, that `10.0.2.15` was port-scanning the machine at `10.0.2.4`. 

![port_scan](7_port_scanning.png) 

That could classify `10.0.2.15` as an attacker. However, to be sure, we have to dig deeper into the traffic. Aside from some HTTP requests that queried the website hosted on `10.0.2.4`, there is some FTP traffic. At this point we can be pretty sure that `10.0.2.15` is the IP of the attacker. We will confirm this information in question 4.

## 3: Open ports
Looking at the port-scan again, it seems that it ended at packet number 1089. Let's filter only packets that got syn/ack from `10.0.2.4`. To do that, I used this filter: `ip.addr == 10.0.2.15 && tcp.flags.ack == 1 && tcp.flags.syn == 1`

![port_scan_result](7_port_scan_result.png)

There are 21 packets that satisfy these conditions and have packet number lower than 1089. So this means that we have 21 open ports that were discovered by the port scan.

## 4: First command they executed
At first it was unclear, whether the attacker got in, or even how did he do it. At first I looked through the HTTP requests, but I did not find anything too suspicious about it. The attacker downloaded some images and queried some pages on the website. 

I opened up the files that he downloaded from the server, however I did not find anything suspicious in the images. The website was a little bit fishy, just looking at the content of the welcome page:
```
HTTP/1.1 200 OK
Date: Wed, 20 Nov 2019 15:07:55 GMT
Server: Apache/2.2.8 (Ubuntu) DAV/2
X-Powered-By: PHP/5.2.4-2ubuntu5.10
Content-Length: 933
Keep-Alive: timeout=15, max=100
Connection: Keep-Alive
Content-Type: text/html

<html><head><title>Metasploitable2 - Linux</title></head><body>
<pre>

                _                  _       _ _        _     _      ____  
 _ __ ___   ___| |_ __ _ ___ _ __ | | ___ (_) |_ __ _| |__ | | ___|___ \ 
| '_ ` _ \ / _ \ __/ _` / __| '_ \| |/ _ \| | __/ _` | '_ \| |/ _ \ __) |
| | | | | |  __/ || (_| \__ \ |_) | | (_) | | || (_| | |_) | |  __// __/ 
|_| |_| |_|\___|\__\__,_|___/ .__/|_|\___/|_|\__\__,_|_.__/|_|\___|_____|
                            |_|                                          


Warning: Never expose this VM to an untrusted network!

Contact: msfdev[at]metasploit.com

Login with msfadmin/msfadmin to get started


</pre>
<ul>
<li><a href="/twiki/">TWiki</a></li>
<li><a href="/phpMyAdmin/">phpMyAdmin</a></li>
<li><a href="/mutillidae/">Mutillidae</a></li>
<li><a href="/dvwa/">DVWA</a></li>
<li><a href="/dav/">WebDAV</a></li>
<li><a href="/tarantino/">Movies</a></li>
</ul>
</body>
</html>
```
To answer the question PaiMei server asked me, I needed commands, that the attacker executed on the target machine. Since I did not find anything that would resemble commandline communication, I stopped looking into the HTML files.

Next, I focused on the FTP traffic:
```
220 (vsFTPd 2.3.4)
USER anonymous
331 Please specify the password.
PASS 
230 Login successful.
SYST
215 UNIX Type: L8
PORT 10,0,2,15,227,235
200 PORT command successful. Consider using PASV.
LIST
150 Here comes the directory listing.
226 Directory send OK.
CWD /
250 Directory successfully changed.
PWD
257 "/"
PORT 10,0,2,15,155,145
200 PORT command successful. Consider using PASV.
LIST
150 Here comes the directory listing.
226 Directory send OK.
QUIT
221 Goodbye.
```
It looks like the attacker got in, but we did not get to the commands he executed. 

Looking through the TCP communication that occurred after the FTP traffic, I found stream 534. The contents of this stream are very interesting:
```
id
uid=0(root) gid=0(root)
nohup  >/dev/null 2>&1
echo XfDmwtRFPzK1NUvL
XfDmwtRFPzK1NUvL
ls
bin
boot
cdrom
dev
etc
home
index.html
initrd
initrd.img
lib
lost+found
media
mnt
nohup.out
opt
proc
root
sbin
srv
sys
tmp
usr
var
vmlinuz
id
uid=0(root) gid=0(root)
ls /home
ftp
msfadmin
service
user
id
uid=0(root) gid=0(root)
exit
```
BINGO! These are console commands and their outputs, that the attacker executed on the server. The first command was `id`.

## Submission
So the final answers we found in the pcap are:
1. 19
2. `10.0.2.15`
3. 21
4. `id`

```
<Pai Mei> So my pathetic friend... is there anything that you can do well?
<Pai Mei> How many HTTP requests did 10.0.2.5 perform?
19
<Pai Mei> What is the IP address of the attacker?
10.0.2.15
<Pai Mei> How many open ports did the port scan find?
21
<Pai Mei> Did the attacker manage to break in the server? (If yes, write the first command they executed, if no answer with No)
id
<Pai Mei> The sacred token lies somewhere inside the HTTP requests. Have you found it?
```
So we got one more question from the submission server. It said that there is some sacred token in the HTTP requests. I searched through some of the HTML files, but could not find anything useful. Submitted some hashes, i found in these files to the submission server, but these were not correct. After trying a bunch, I realized, that the token is probably not in the HTML files. 

As I did not search through all of the images, I searched through them once again. The last image in the traffic, `jimmy.png` looked like this:

![7_wolf](7_wolf.png)

Using the token `JimmyBuysGreatCoffee` on the submission server got us the flag:

```
<Pai Mei> The sacred token lies somewhere inside the HTTP requests. Have you found it?
JimmyBuysGreatCoffee
<Pai Mei> It's time to learn the Five-Point-Palm Exploding-Heart-Technique: BSY-FLAG-A08-{aCFAD08Df5Db85Fdc6ceF26a32B3Ce112b9d4fd9c6D66eECdfbe29ABE94Ab9Cf}
```