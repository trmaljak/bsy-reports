# Lab3
The task was to observe traffic in the network, find some periodic event and find the token. 

## Capturing traffic
First, I started packet capture on my machine. Running command `sudo tcpdump -n -s0 -i enp0s3 -v -w a.pcap port ! 22`. I excluded port 22, to filter out SSH traffic, which reduced the size of the dump considerably. I let this command run for a couple of hours and then inspected the contents of the `a.pcap` file.

## Analysis
In the capture there was a lot of traffic, mainly port scans from machines of our colleagues, so the packet capture got really big, really fast. I had to run tcpdump multiple times in order to be sure, that I got the event in the capture. 

To save me some time, I filtered out all TCP traffic from the dump using filter `! tcp`. This might have filtered the event out from the dump, but I did not want to deal with all the port-scanning traffic. Only when I would have searched through all of the non-tcp traffic, I would then return to the tcp.

In the filtered dump, there were lots of **ARP** and **ICMP** packets. After a brief ad-hoc inspection I decided to filter them out, because there was nothing too suspicious in there:
`!tcp && !icmpv6 && !arp`.

There was an unusual amount of UDP packets in the packet capture. Just browsing through these packets, I noticed a peculiar periodic sequence of colors in wireshark:

![3_peculiar_colors](3_peculiar_colors.png)

The traffic in these areas was interesting. There were UDP packets coming from `192.168.1.167` to port 83. This port was closed on my machine, so every packet was immediately followed by an ICMP packet, telling the sender, that the destination was unreachable. I filtered the traffic further using `ip.src == 192.168.1.167`. Now the pattern was pretty obvious:

![3_obvious_message](3_obvious_message.png)

I opened one of these packets and it was immediately obvious, that there was something hidden in there:
```
2/12: BEEP BEEEEEEP BEEP
```
Unfortunately I could not figure out, how to display all of the packet data at once, so I had to go through all of the packets individualy and extract the data. I noticed that the data was repeating with the period of about one hour. I picked one of those periods to analyze, however there were some packets missing in there. I filled the missing pieces from other periods and assembled the complete message:

```
KNOCK KNOCK. ANYBODY HERE? SENDING DATA!
1/12: BEEEEEEP
2/12: BEEP BEEEEEEP BEEP
3/12: BEEP BEEP
4/12: BEEEEEEP BEEP BEEEEEEP BEEP
5/12: BEEEEEEP BEEP BEEEEEEP
6/12: BEEEEEEP BEEEEEEEP BEEEEEEEP
7/12: BEEP BEEEEEEEEP BEEP
8/12: BEEEEEEP
9/12: BEEP BEEEEEEEEP BEEP
10/12: BEEP
11/12: BEEP BEEEEEEEP
12/12: BEEEEEEEEP
END OF STORY. Reply using TCP in port 8000
```

The data is a bit cryptic. Just looking at it, I could tell that it could represent some number. After converting the message to bits (long beep for 1, short beep for zero) I got this:

```
1/12: 1        - 1
2/12: 0 1 0    - 2
3/12: 0 0      - 0
4/12: 1 0 1 0  - 10
5/12: 1 0 1    - 5
6/12: 1 1 1    - 7
7/12: 0 1 0    - 4
8/12: 1        - 1
9/12: 0 1 0    - 2
10/12: 0       - 0
11/12: 0 1     - 1
12/12: 1       - 1
```

I did not get much information doing this. There were some combinations that did not make any sense. For example in message 2, there were two zeros. If the message was supposed to be zero, it would be enough to use only one zero. 

Then I realized that I might have misunderstood the format. Looking at the original message, I remembered my childhood, as my father was teaching me essential skills for my future life. One of those skills was morse code. With some help from the [unit converted](http://www.unit-conversion.info/texttools/morse-code/), I found some token:
```
- .-. .. -.-. -.- --- .-. - .-. . .- -
trickortreat
```

Now, in the last packet of the message, it told me to reply using tcp in port 8000. As the messages came from `192.168.1.167`, I used netcat to connect to this IP on port 8000 and got the flag as a result:

```{shell}
class@ubuntu8:~$ ncat 192.168.1.167 8000
                     @@@
                     @@@
                      @@@                       H A P P Y
                      @@@
              @@@@@@@@@@@@@@@@@@@@@@         H A L L O W E E N
            @@@@@@@@@@@@@@@@@@@@@@@@@@
          @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @@@@@@@@ @@@@@@@@@@@@@@@@ @@@@@@@@
      @@@@@@@@@   @@@@@@@@@@@@@@   @@@@@@@@@
    @@@@@@@@@@     @@@@@@@@@@@@     @@@@@@@@@@
   @@@@@@@@@@       @@@@  @@@@       @@@@@@@@@@
   @@@@@@@@@@@@@@@@@@@@    @@@@@@@@@@@@@@@@@@@@
   @@@@@@@@@@@@@@@@@@        @@@@@@@@@@@@@@@@@@
   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   @@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@
    @@@@@@@@  @@ @@ @@ @@ @@ @@ @@ @  @@@@@@@@
      @@@@@@@                        @@@@@@@
        @@@@@@  @@ @@ @@ @@ @@ @@ @ @@@@@@
          @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            @@@@@@@@@@@@@@@@@@@@@@@@@@
              @@@@@@@@@@@@@@@@@@@@@@

------------------------------------------------
This ASCII pic can be found at
https://asciiart.website/index.php?art=holiday/halloween



********************************************************************
Flag server for assignment 3. Not relevant to the second assignemnt.
********************************************************************


<Server> Please give me your token!
trickortreat
<Server> You did good, here is your treat: BSY-FLAG-A03-{0572Ee133Aadf88dee1C6f9dAE1ccFEF6fD87D02F8F8Bcd4cd2EF22658f22aB2}
```

